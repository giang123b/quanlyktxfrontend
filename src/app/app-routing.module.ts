import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SinhVienListComponent } from './components/list/sinh-vien-list.component';
import { SinhVienDetailsComponent } from './components/details/sinh-vien-details.component';
import { SinhVienAddComponent } from './components/add/sinh-vien-add.component';

const routes: Routes = [
  { path: '', redirectTo: 'sinhviens', pathMatch: 'full' },
  { path: 'sinhviens', component: SinhVienListComponent },
  { path: 'sinhviens/:id', component: SinhVienDetailsComponent },
  { path: 'addsinhvien', component: SinhVienAddComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
