import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SinhVienAddComponent } from './components/add/sinh-vien-add.component';
import { SinhVienDetailsComponent } from './components/details/sinh-vien-details.component';
import { SinhVienListComponent } from './components/list/sinh-vien-list.component';

@NgModule({
  declarations: [
    AppComponent,
    SinhVienAddComponent,
    SinhVienDetailsComponent,
    SinhVienListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
