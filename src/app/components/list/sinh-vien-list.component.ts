import { Component, OnInit } from '@angular/core';
import { SinhVienService } from 'src/app/services/sinh-vien.service';

@Component({
  selector: 'app-tutorials-list',
  templateUrl: './sinh-vien-list.component.html'
})
export class SinhVienListComponent implements OnInit {

  sinhViens: any;
  sinhVienHienTai = null;
  currentIndex = -1;
  title = '';

  constructor(private sinhVienService: SinhVienService) { }

  ngOnInit(): void {
    this.retrieveSinhVien();
  }

  retrieveSinhVien(): void {
    this.sinhVienService.getAll()
      .subscribe(
        data => {
          this.sinhViens = data;
          console.log('data', data);
        },
        error => {
          console.log('error', error);
        });
  }

  refreshList(): void {
    this.retrieveSinhVien();
    this.sinhVienHienTai = null;
    this.currentIndex = -1;
  }

  setActiveSinhVien(tutorial, index): void {
    this.sinhVienHienTai = tutorial;
    this.currentIndex = index;
  }

  removeAllSinhVien(): void {
    this.sinhVienService.deleteAll()
      .subscribe(
        response => {
          console.log(response);
          this.refreshList();
        },
        error => {
          console.log(error);
        });
  }

  searchTitle(): void {
    this.sinhVienService.findByHoTen(this.title)
      .subscribe(
        data => {
          this.sinhViens = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }
}
