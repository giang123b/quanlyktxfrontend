import { Component, OnInit } from '@angular/core';
import { SinhVienService } from 'src/app/services/sinh-vien.service';

@Component({
  selector: 'app-add-tutorial',
  templateUrl: './sinh-vien-add.component.html'
})
export class SinhVienAddComponent implements OnInit {
  sinhVien = {
    hoTen: '',
    maSinhVien: '',
    soCMT: '',
    ngaySinh: '',
    lop: '',
    queQuan: ''
  };
  submitted = false;

  constructor(private tutorialService: SinhVienService) { }

  ngOnInit(): void {
  }

  saveTutorial(): void {
    const data = {
      hoTen: this.sinhVien.hoTen,
      maSinhVien: this.sinhVien.maSinhVien,
      soCMT: this.sinhVien.soCMT,
      ngaySinh: this.sinhVien.ngaySinh,
      lop: this.sinhVien.lop,
      queQuan: this.sinhVien.queQuan,
    };

    this.tutorialService.create(data)
      .subscribe(
        response => {
          console.log(response);
          this.submitted = true;
        },
        error => {
          console.log(error);
        });
  }

  newTutorial(): void {
    this.submitted = false;
    this.sinhVien = {
      hoTen: '',
      maSinhVien: '',
      soCMT: '',
      ngaySinh: '',
      lop: '',
      queQuan: ''
    };
  }

}
