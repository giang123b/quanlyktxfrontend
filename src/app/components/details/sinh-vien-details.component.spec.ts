import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SinhVienDetailsComponent } from './sinh-vien-details.component';

describe('TutorialDetailsComponent', () => {
  let component: SinhVienDetailsComponent;
  let fixture: ComponentFixture<SinhVienDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SinhVienDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SinhVienDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
