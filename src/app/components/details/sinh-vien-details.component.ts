import { Component, OnInit } from '@angular/core';
import { SinhVienService } from 'src/app/services/sinh-vien.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-tutorial-details',
  templateUrl: './sinh-vien-details.component.html'
})
export class SinhVienDetailsComponent implements OnInit {
  currentSinhVien = null;
  message = '';

  constructor(
    private tutorialService: SinhVienService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.message = '';
    this.getSinhVien(this.route.snapshot.paramMap.get('id'));
  }

  getSinhVien(id): void {
    this.tutorialService.get(id)
      .subscribe(
        data => {
          this.currentSinhVien = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

  updateSinhVien(): void {
    this.tutorialService.update(this.currentSinhVien.maSinhVien, this.currentSinhVien)
      .subscribe(
        response => {
          console.log(response);
          this.message = 'The tutorial was updated successfully!';
        },
        error => {
          console.log(error);
        });
  }

  deleteSinhVien(): void {
    this.tutorialService.delete(this.currentSinhVien.maSinhVien)
      .subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/sinhviens']);
        },
        error => {
          console.log(error);
        });
  }
}
